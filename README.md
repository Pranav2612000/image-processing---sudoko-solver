# Image Processing - Sudoko Solver

Fully functioning Sudoko Solving application, which when provided with a Sudoku image solves and displays the Sudoku. Implemented the project in Python and used "OpenCV" for image recognition.

Instructions to Run:
1)Workon cv
2)python3 gui.py